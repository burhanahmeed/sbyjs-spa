import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router';

Vue.use(VueRouter);
Vue.config.productionTip = false

const routes = [
  {
    path: '/',
    component: App,
    meta: {
      title: 'Home Page of SPA Hello from SBYJS',
      metaTags: [
        {
          name: 'description',
          content: 'Ini adalah meta tag dari website ini. Ini adalah website single page application.'
        },
        {
          property: 'og:description',
          content: 'Ini adalah meta tag dari website ini untuk Facebook open graph.'
        },
        {
          property: 'og:image',
          content: 'https://images.ctfassets.net/3prze68gbwl1/asset-17suaysk1qa1jtx/b1e3eda412fbc2c2be1f6d6229605ed0/node-js-websockets-programming.png'
        }
      ]
    }
  },
  {
	path: '/tes',
	component: App,
	meta: {
      title: 'Tes Page of SPA Hello from SBYJS',
      metaTags: [
        {
          name: 'description',
          content: 'Ini adalah meta tag dari Halaman tes page.'
        },
        {
          property: 'og:description',
          content: 'Ini adalah meta tag dari Halaman tes page untuk Facebook open graph.'
        },
        {
          property: 'og:image',
          content: 'https://images.ctfassets.net/3prze68gbwl1/asset-17suaysk1qa1jtx/b1e3eda412fbc2c2be1f6d6229605ed0/node-js-websockets-programming.png'
        }
      ]
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // eg. if we have /some/deep/nested/route and /some, /deep, and /nested have titles, nested's will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  // const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  // If a route with a title was found, set the document (page) title to that value.
  if(nearestWithTitle) document.title = nearestWithTitle.meta.title;

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

  // Skip rendering meta tags if there are none.
  if(!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  nearestWithMeta.meta.metaTags.map(tagDef => {
    const tag = document.createElement('meta');

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key]);
    });

    // We use this to track which meta tags we create, so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '');

    return tag;
  })
  // Add the meta tags to the document head.
  .forEach(tag => document.head.appendChild(tag));

  next();
});


new Vue({
  el: '#app', router, render: h => h(App)
}).$mount('#app')
